# load 'hello_from_file.rb'

# puts "Hello"
# p "Hello with the shortform of puts"
# print "print Prints to the screen but does not put a carriage return at the end of the line"

# For a single line comment you use #

# For multi line comments you use =begin to start it and you end it with =end

=begin
Like
This is a comment
that is followed
by many
other lines
=end

# Variables
# name = "Allan"
# puts name

# number = 5
# puts number

# condition = true

# puts name, condition, number
# print name, ' ', condition,  ' ', number, '.'
# puts '----------------------------------------------------------------'

# To print a variable in a string statement
# puts "Hello #{name}"

#Get user input
print 'Enter your name: '
name = gets.chomp

# To convert input to interger use get.chomps.to_i
print "What is your ID number: "
id = gets.chomp.to_i
puts "Hello #{name}, your ID number is #{id}"
