# times: used on number

3.times do
  puts "Hooray!"
end

# upto: iterate till a given limit
5.upto(10 ) do |num|
  puts num
end

result = 0
1.upto(10) do |num|
  puts result += num

end
puts result

# downto: opposite of upto. Can be used for a countdown timer for example