# Ranges can be used to do iteration. Range with two dots are inclusive, it includes the last number
# Instead of using upto we can use a range
(1..10).each do |num|
  puts num
end

# To make a range exclusive use 3 dots ...
range_integer_ = 1...10
(range_integer_).each do |num|
  puts num
end

#To convert a range to an array use .to_a
(1..7).to_a
('a'..'z').to_a
(1.3..2.7).to_a # This can't work as(1.3..2.7).to_a floats represent numbers across the numberline so the array would be infontisimal

# ranges have begin and end
a = 1..10
puts a.end # =>10
puts a.begin # =>1
a.exclude_end? # => false   This checks if the final element has been excluded