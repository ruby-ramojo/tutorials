# Print range from A to K
letter_range = 'A'..'K'
puts letter_range.to_a
# OR
('A'..'K').each do |letter|
  puts letter
end
# OR
('A'..'K').each { |letter| puts letter }

# Determine if float is between
def is_float_between?(range_given, num)
  # (0..10.9).each do |num|
  #   num>= range_given.begin && num <= range_given.end
  # end
  # num = 7
  if range_given.exclude_end?
    num >= range_given.begin && num < range_given.end
  else
    num >= range_given.begin && num <= range_given.end
  end
end
#OR
# Range has a method to do all the above called .cover?
def is_float_between2(range_given, num)
  range_given.cover?(number)
end

puts is_float_between?(1.1..10.9, 7)

#Multiples of 7 between 7 and 140
multiples = 0
(1..20).each do |num|
  puts multiples = num * 7
end

# See if two ranges intersect
def intersection_ranges(range1, range2)
  if range1.begin < range2.begin
    if range1.end < range2.begin
      nil  # return nil
    else
      (range2.begin..range1.end)
    end
  else
    if range2.end < range1.begin
      nil
    else
      (range1.begin..range2.end)
    end
  end
end