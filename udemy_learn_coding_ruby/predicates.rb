# Predicates are if, unless, if...else statements
# Flow control and conditions are things like < > = != etc

=begin
if condition
    # do something
end
=end
# orit can be writen as: 
# do something if condition e.g. 

temp = 30

puts "It is hot!" if temp > 30

# This can be rewritten as: 
if temp > 30
    puts "It is hot!"
end

=begin
unless condition
    # do something
end

=end
# e.g.
unless temp < 30
    puts "It is hot!"
end

=begin
if condition
    # do something
else
    # do something else
end
=end
# e.g. 

if temp >= 30
    puts "It is hot!"
else
    puts "It is actually kinda cold"
end