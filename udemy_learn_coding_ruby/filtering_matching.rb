# Replace characters
# We use gsub
puts "abracadabra".gsub('a', 'A') # First argument is what you want to replace and second is what you want to replace it to
puts "abracadabra".gsub('a', '')
puts "abracadabra".gsub('abracadabra', 'Magic! They\'re gone')
# Note there's also gsub! to replace the original string
str = "testing"
str.gsub!('e', '')
puts str