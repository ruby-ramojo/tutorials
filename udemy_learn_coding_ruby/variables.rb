# Variables must start with a lowercase letter
# Underscores are ok anywhere, but dashes are not

vartest = "1"
vartest2 = "2"
_vartest = "3"
__ = "4" # You can even have underscores as a variable name!

# There's also something called a literal. A literal is a specific value while a variable can change

# Numbers
x = 314 # integer

3.14 # Literal float

y = 3.14 # Float

# Convert string to integer
"100".to_i # this becomes the integer 100

# You can also convery an integer back to a string by using interpolation on either the integer itself or the variable holding that integer
puts " #{x} to a string "

# Convert a string to float
100.to_f # Becomes 100.0