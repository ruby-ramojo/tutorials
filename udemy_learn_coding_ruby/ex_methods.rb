# Add 2 numbers

def add_two_numbers(num1,num2)
    return num1 + num2 # In ruby you don't have to return a value at the end of a method
end

puts add_two_numbers(4,3)

# Multiply 3 numbers
def multiply_three_numbers(num1,num2,num3)
    num1 * num2 * num3 #
end

puts multiply_three_numbers(4,7,2)

# Return string with interpolated values
def temperature(degrees)
    return "The temperature outside is #{degrees} deg Celcius"
end

puts temperature(28)

#Return a description of the above temperature provided e.g. 'It's hot'
def temperature_desc(degrees)
    if degrees >=30
        return "The temperature outside is #{degrees} deg Celcius. It's Hot!"
    else
        return "The temperature outside is #{degrees} deg Celcius. It's Cold!"
    end
end

puts temperature_desc(28)
