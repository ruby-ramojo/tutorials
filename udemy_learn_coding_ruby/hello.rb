puts 'Hello, please tell me your name'
prefix = "Mr."
# name = gets
# To avoid the return at the end of the gets method, which leads to a new line, we use gets.chomp
name = gets.chomp
puts "Your name is #{prefix} #{name} :-) " # using #{variable} is called interpolation. This inserting the value of a variable into a string
# Note that single quotes do not interpolate. We use single quotes if you want to have double quotes in the string to be printed e.g.

puts 'Today is "AWESOME!!!" and I like it!'
# This can also be done using escape characters if you use double quotes
puts "Today is \"AWESOME\" and I like it!"

print "This also works if we use \"print\" instead of \"puts\""