str = "The quick brown fox jumped over the lazy dog."
# Reverse
puts str.reverse
# Capitalize
puts str.upcase
# Count
puts str.length
puts str.size
# New string which is the first 10 characters
puts str[0..9]
# New string which is the last 2 characters
puts str[-2..-1]
# Determine the 5th character
str[4]