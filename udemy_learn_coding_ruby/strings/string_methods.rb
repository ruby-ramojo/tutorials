# Reverse
# Can be performed on a string literal or on a string saved in a variable
puts "How are you doing?".reverse
hello = "Hi there!"
puts hello.reverse

# Uppercase
puts hello.upcase

# Length
# Use .length or .size they do the same thing. This is actually an attribute, not a method
puts hello.size
puts hello.length

# Indexing. This gets the character at the position of the index
puts hello[3] # start counting from 0, not 1. t is the fourth character but carinaly it is in the 3 position

# Slicing means getting like a range.
puts hello[3..5]

# Negative indices- We count from the end of the string backwards. Note we don't start from 0 here as these are before 0
puts hello[-3]
puts hello[-5..-3]