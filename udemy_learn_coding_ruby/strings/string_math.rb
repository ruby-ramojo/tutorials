# Concatenation: Adding strings together
puts "Allan " + "Ramogo"

# Multiplication
puts "test" * 3

# Mutation: Adding a bang to methods e.g. reverse! upcase! This changes the variable value even if you were assigning it to something else
str1 = "hello" # hello
str2 = str1.reverse! # Both str1 and str2 will now be olleh

