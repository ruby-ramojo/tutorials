# A hash can be thought of as an arrays' big brother
# if an array is a list a hash is a library, if an array is a phone number a hash is a phonebook
# A hash allows you to associate a key to a value, otherwise known as a dictionary

ages = { 'Sam' =>7, 'Sarah' => 10, 'Kumar' => 12 }
# => is called hash rocket and is used to assign a value to a key

ages['Sarah'] # Will output 10 is asked to puts the returned value.
# The names are the keys and the ages are the values
# Hashes are meant to be unique

# You can add and overwrite hashes
ages['Ron'] = 12
ages['Ron'] # Will give age as 12

# Overwrite
ages['Ron'] = 14
ages['Ron'] # Will give age as 14

# Example:
scores= {}
scores['knicks'] = 77
scores['knicks'] # Returns 77 if asked for output
scores['knicks'] = 85
# Since hashes are unique each key can only have one value associated with it
# This is wht the value gets replaced in the example above
# Just like arrays, hashes can contain anything, even other hashes
#
# if there's no value set to a key, nil is returned
hash = {}
hash['a'] # => nil

# You can check if a hash exists by using include?, just like in arrays
scores.include?('bulls') # Returns false as bulls doesn't exist in this hash

# ITERATE A HASH
# You have to pass two variables to the block, not one like we do with arrays
# Also you use each_pair instead of each
products = {
  'Apple' => 0.75,
  'Orange' => 1.5
}
products.each_pair do |key, value|
  puts "#{key} is $#{value}"
end