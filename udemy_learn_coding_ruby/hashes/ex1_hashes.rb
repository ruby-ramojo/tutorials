# Create hash containing people and their bank balances
bank_balances = {
  'Teko' => 9785,
  'Wendo' => 65345,
  'Shery' => 87654,
  'Allan' => 876654
}
sum = 0
puts "Everyone and their bank balances:"
puts "Name              Balance"
=begin
bank_balances.each_pair do |name, balance|

  puts "#{name} -----------  Ksh. #{balance}"
end

# Add all the values in the hash
sum = 0
bank_balances.each_pair do |name, balance|
  sum += balance
end
puts " The total everybody has is Ksh. #{sum}"
=end
# The two top iterations can be combined:
bank_balances.each_pair do |name, balance|

  puts "#{name} -----------  Ksh. #{balance}"
  sum += balance

end
puts "The total everybody has is Ksh. #{sum}"