# Access the keys in a hash as an array
hash = {1=> 'a', 2 => 'b'}
puts hash.keys

# Access the values in a hash as an array
puts hash.values

# Hashes adopt a natural ordering- insert a new key-value pair, the order stays in the order you inserted it
# Yuo can sort this though
# Sort 1: the keys in the hash
hash2 = { 3 => 'c', 1 => 'a', 2 => 'b'}
hash2.keys.sort.each do |key|
  puts "#{key} : #{hash2[key]}"
end

# Create a new hash which is the sorted version of the previous hash
sorted_hash = {}
hash2.keys.sort.each do |key|
  sorted_hash[key] = hash2[key]
end
puts sorted_hash

# Third way to sort

sorted_array = hash2.sort do |(key1,val1), (key2,val2)|
  key1 <=> key2 # We get 2 element arrays e.g [1, 'a'], [2, 'b'], [3, 'c']
end
sorted_hash2 = Hash[sorted_array]
puts sorted_hash2