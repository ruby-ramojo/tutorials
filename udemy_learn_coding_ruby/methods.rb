# You create your own method with the def keyword and close it with end
def my_method
    puts "This is my first test method"
end

# the method can be called by using its ame anywhere in the code
my_method

name = "Baba Teko" # No need to have this here. It will still work without it but it shows that the name acually changes It can work without declaring a variable
puts "The original name is #{name}"
def method_2(name)
    puts "My name is #{name}"
end

method_2("Allan")
# Can also be achieved without the brackets
method_2 "Baba Wendo"
# This approach is not encouraged though but it is how we use the puts method

def personal_info(name, age)
    puts "Name: #{name} "
    puts "Age: #{age}"
end

personal_info("Mama Teko", "32")

puts "What's your name? "
name = gets.chomp
puts "How old are you? "
age = gets.chomp
personal_info(name, age)