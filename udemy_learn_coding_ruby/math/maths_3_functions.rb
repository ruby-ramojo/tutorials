def area(radius)
  display((Math::PI) * (radius.to_f ** 2))
end

puts "Please type the radius of the circle"
rad = gets.chomp.to_f

area(rad)
