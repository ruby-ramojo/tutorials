# To put decimal places after the decimal point, for example, we use sprintf(%.2f, number). This method is a throwback from C
puts sprintf("%.3f", 34.46789)
