# Divide integer with another integer
# Converting the integers to floats is called casting

def integer_divide(int1, int2)
  int1.to_f/int2.to_f
end

puts integer_divide(7, 4)