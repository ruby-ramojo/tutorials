# Rounding by converting to integer. This just eliminates the floating point number, not proper rounding

num1 = 4.7
num2 = 4.3
puts num1.to_i

# Rounding is done using the round method e.g.
puts num1.round

# You can also use floor to round down and ceil to round up. Even 4.3 rounded using ceil will give us 5
puts num2.floor
puts num1.ceil
puts num2.ceil