# Simple Calculator
def add(num1, num2)
  display(num1 + num2)
end

def subtract(num1, num2)
  display(num1 - num2)
end

def multiply(num1, num2)
  display(num1 * num2)
end

def divide(num1, num2)
  display(num1 / num2)
end

def power(num1, num2)
  display(num1 ** num2)
end
def square_root(num)
  display(Math.sqrt(num))
end

def area_sphere(diameter)
  # pi * diameter^2
  display(Math::PI * (diameter ** 2))
end

def length_hypotenuse(num1, num2)
  # a^2 = b^2 + c^2
  # Both these work but the hypot function is coolers
  display(Math.hypot(num1, num2))
  # display(Math.sqrt(num1**2 + num2**2))
end

def display(result)
  puts sprintf("%.2f", result)
end


puts "Please enter the operation: add, subtract, multiply, divide, power, sqrt, area-sphere, hypot"
operation = gets.chomp

puts "Please enter the first number"
number1 = gets.chomp.to_f
if operation != 'sqrt' && operation != 'area-sphere'
  puts "Please enter the second number"
  number2 = gets.chomp.to_f
end

# if operation =="add"
#   puts add(number1, number2)
# elsif operation =="subtract"
#   puts subtract(number1, number2)
# elsif operation =="multiply"
#   puts multiply(number1, number2)
# elsif operation =="divide"
#   puts divide(number1, number2)
# elsif operation =="power"
#   puts power(number1, number2)
# else
#   puts "Invalid input"
# end
# We can use inline if statements to make this more readable
add(number1, number2) if operation =="add"
subtract(number1, number2) if operation =="subtract"
multiply(number1, number2) if operation =="multiply"
divide(number1, number2) if operation =="divide"
power(number1, number2) if operation =="power"
square_root(number1) if operation == "sqrt"
area_sphere(number1) if operation == "area-sphere"
length_hypotenuse(number1, number2) if operation == "hypot"
