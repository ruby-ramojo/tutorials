=begin
Operators are +, -, *, /, **(this is for exponents end 2**3 is 2^3), %(modulo gives you the remainder)
Multiplication of int * float = float
Division of int/int = int e.g. 1/4 = 0
=end

#Method to divide and integer with another integer
def divide_integers(num1, num2)
    return num1/num2
end

print "Type the first number: "
numb1 = gets.chomp
print "Type the second number: "
numb2 = gets.chomp
puts divide_integers(numb1.to_i, numb2.to_i)