input = <<-STR
Stacy Brown-Philpot is the chief operating officer of TaskRabbit, where she’s responsible for scaling and expanding the marketplace. Before TaskRabbit, she spent nearly a decade leading global operations for Google’s flagship products. She served as Head of Online Sales and Operations for Google India and opened the office in Hyderabad. Stacy was also an entrepreneur in residence at Google Ventures.
STR

$words = input.split(/\s+|\.|\,/).select { |w| !w.empty? } # This uses a Regular Expression (we'll learn about those later)
$words2 = input.split(/\s+|\.|\,/).select { |w| !w.empty? } # This uses a Regular Expression (we'll learn about those later)

# $words is a special variable called a global variable (indicated by the $)
# It is an array of all the words in the input string and you can use it to perform this exercise
#
# For example to access the 5th word in the array do:
#
#    $words[4]
#
# Your code starts here

#Print each word on one line
$words.each do |word|
  puts word
end
#Or
$words.each { |word| puts word }
puts '+++++++++++++++++++++++++++++++++++++++++'
# Sort words and print again
$words.sort.each do |word|
  puts word
end
# you can also do:
=begin
$words.sort!
$words.sort.each do |word|
  puts word
end
=end

#Sort and reverse and print
$words.sort.reverse.each do |word|
  puts word
end
# Can also use $words = $words.reverse!
#
# Add word, remove first element, sort and delete
$words << "Allan"
$words.delete_at(0)

$words.sort.each { |word| puts word }

# All this can be accomplished another way:
def display(array)
  array.each do |elem|
    puts elem
  end
end

#Print each word on one line
display($words2)

# Sort words and print again
sorted = $words2.sort
puts "\n\nSorted\n"
display(sorted)

#Sort and reverse and print
puts "\n\nSorted Reverse\n"
display(sorted.reverse)

# Add word, remove first element, sort and delete
sorted << "Teko"
sorted.delete_at(0)
display(sorted.sort)