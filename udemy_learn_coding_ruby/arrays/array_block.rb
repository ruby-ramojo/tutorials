# result = [ 'gum', 'pine', 'palm', 'olive' ]

#Find: find the first element with first letter o
result = [ 'gum', 'pine', 'palm', 'olive' ].find do |tree|
  tree[0] == 'o' # means look for the first character
end

puts result

#Select: find all elements that meet the condition, not just the first
result = [ 'gum', 'pine', 'palm', 'olive' ].select do |tree|
  tree[0] == 'p' # means look for the first character
end

puts result

# Mapping: Create a new array by mapping the array and modifying each element as we go
# Uppercase all elements
birds = %w(magpie bower crow eagle).map do |bird|
  bird.upcase
end
puts birds

# Sort with a block. We use a comparison operator <=>. It allows us to see which object is bigger out od a pair
# 1 <=> 1 gives us 0 coz the objects are the same
# 1 <=> -1 gives us 1 since the first object is bigger
# 1 <=> 2 gives us 1 since the second object is bigger
# In this example we are telling sort not to use its own lexigraphical sort buy to use our own custom one instead
# It uses the quick sort algorithm to do this
cars = %w(toyota subaru vw honda ford).sort do |a,b|
  a.length <=> b.length
end
puts cars
# We can reverse the order by reversing the operands
cars2 = %w(toyota subaru vw honda ford).sort do |a,b|
  b.length <=> a.length
end
puts cars2

# We can use each_with_index to add an index to the array
dogs = %w(poodle labrador bulldog pitbull pug).each_with_index do |breed, index|
  puts "#{index} : #{breed}"
end

# Arrays can also contain other arrays
pets = [ [ 'cat', 14 ], [ 'dog', 7 ] ] # This example has pet and age
# To access the first element
puts pets[0]
# To access the first element of the first element
puts pets[0][0]
# Just works like a matrix in maths