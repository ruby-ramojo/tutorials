arr = %w(a b c c d e f)
puts arr
puts "--------------------------------------------------"
# 3rd element
puts arr[2]

# Retrieve first two elements
puts arr[0..1]

# Retrieve the last element
puts arr[-1]

# Delete duplicate c
array_deleted =  arr.delete_at(2)
puts arr

# Add character g to end
arr << "g"
puts arr

