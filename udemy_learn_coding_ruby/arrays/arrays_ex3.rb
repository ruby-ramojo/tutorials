input = <<-STR
Stacy Brown-Philpot is the chief operating officer of TaskRabbit, where she’s responsible for scaling and expanding the marketplace. Before TaskRabbit, she spent nearly a decade leading global operations for Google’s flagship products. She served as Head of Online Sales and Operations for Google India and opened the office in Hyderabad. Stacy was also an entrepreneur in residence at Google Ventures.
STR

$words = input.split(/\s+|\.|\,/).select { |w| !w.empty? } # This uses a Regular Expression (we'll learn about those later)
def occur(word)
  arr = $words.select do |word_sample|
    word == word_sample
  end
  arr.size
end

frequencies = $words.uniq.map do |word|
  [ word, occur(word) ]
end

sorted_freq = frequencies.sort do |a, b|
  # puts "comparing #{b[-1]} to #{a[-1]}"
  b[-1] <=> a[-1]
  # b[1] <=> a[1] same as the first
end
sorted_freq.each_with_index do |pair, index|
  puts "#{index+1}: #{pair[0]} #{pair[1]} occurrences"
end
# puts occur("the")
# $words = $words.uniq
# $words.each_with_index do |word, index |
#   puts "#{index} : #{word} " + occur(word).to_s + " occurrences"
# end
