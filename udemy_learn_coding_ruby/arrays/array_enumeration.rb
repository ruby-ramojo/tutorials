# Enumerable means we can iterate or enumerate them
animals = %w(dog pig horse hen)

# Using the old for method
for animal in animals
  puts animal
end

# New method called block iterator
animals.each do |animal|
  puts animal
end
# Or use this method below, same thing but best for just one statement. Don't use curly braces for more than one coz it gets messy
animals.each { |animal|
  puts animal
}

#Test for true or false
puts animals.include?('dog') # tests to see if dog is a value in the array. If yes print True
puts animals.include?('donkey') # will be false

# Size
puts animals.size

# Sort- It sorts them in order. Uses lexicographic sorting- numbers then capitals then small letters
puts animals.sort

# Reverse
puts animals.reverse

# Unique- selects the unique numbers and leaves out the repeated copies
numbers = [3, 5, 5, 2, 1, 1, 2, 4] # selects 3 5 2 1 4
puts numbers.uniq

# Methods on arrays can also be chained
puts numbers.sort.reverse