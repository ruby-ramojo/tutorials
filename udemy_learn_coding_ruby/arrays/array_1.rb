# Array is a collection of objects
numbers = [ 1, 2, 3, 4, 5 ]

puts numbers

# a string array has a shortcut: %w( value1 value2, value3) which is just the same as saying [ 'value1', 'value2', 'value3' ]
# For strings with spaces you have to use the square bracket notation with quotation marks
pets = %w(cat dog pig)
puts pets
children = [ 'Luke Teko', 'Olivia Wendo' ]
puts children

# Ruby can have mixed types all inside the same array i.e. strings, integers, floats
mixed = [1, "Allan", 3.14]
puts mixed

# To access an element in an array you use cardinal indexing
puts mixed[1]
# You can append and delete elements in an array
mixed << 5
puts mixed
mixed.delete(5) # This is the element being deleted, not its index
puts mixed

mixed.delete_at(1) # This deletes the element at the specified index
puts mixed