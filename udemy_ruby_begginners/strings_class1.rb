# Class 1
name = "John Doe"
puts name
puts name.class

address = 'Test address using single quotes instead of double'
puts address
puts address.class

# Multi-line Comments
=begin
Line 1
Line 2
Line 3
=end

# Using print
# puts - displays output in new line
# print - displays output in same line
city = 'Nairobi'
print city

subcounty = "Kasarani"
print subcounty